# Environnement de développement Progression

## Prérequis

- [docker](https://www.docker.com/)

## Obtenir les sources

Cloner ce dépôt et les sous-modules (progression_backend et progression_frontend) :

`git clone --recurse-submodules https://git.dti.crosemont.quebec/progression/dev progression`

Exécuter les commandes suivantes dans le dossier progression
`cd progression`

Mettre à jour les sous-modules :
`git submodule update --remote --merge`

## Configuration

La configuration de l'environnement de développement se fait dans le fichier .env

- renommez (ou copiez) env.exemple en .env
- Si nécessaire, ajustez ces variables selon les adresses et port réels :

  - APP_PORT=8080 # Port du service API
  - APP_URL=http://localhost:8080 # URL complet de l'API
  - VITE_API_URL=http://localhost:8080 # URL de l'API (devrait être identique à APP_URL)
  - VITE_APP_PORT=80 # Port du service web
  - TTYSHARE_PROXY_PORT=8081 # Port du proxy TTY-Share

### Obtenir les logs PHP localement

Pour monter le répertoire de logs localement, il faut d'abord créer le répertoire local et donner les permissions d'écriture à tous. Les instructions complètes se trouvent dans le fichier docker-compose.yml sous «Répertoire de logs».

## Démarrage du serveur d'API (backend)

- `docker compose up -d api`

## Démarrage de l'application web (frontend)

- `docker compose up -d webapp`

## Démarrage de compilebox (exécuteur)

- `docker compose up -d compilebox`

## Tester l'installation

### Tester l'API

L'API devrait être accessible à l'adresse $APP_URL (par défaut http://localhost:8080)

```
$ curl http://localhost:8080
Progression 2.6.0()
```

### Tester l'application web

L'application devrait être accessible sur le port $VITE_APP_PORT (par défaut 80)

```
$ curl -i http://localhost:80
HTTP/1.1 200 OK
Access-Control-Allow-Origin: *
Content-Type: text/html
Cache-Control: no-cache
...
```


### Tester la compilation

* L'API utilise une cache qui évite de soumettre inutilement deux fois le même code à l'exécuteur. Pour être certain de tester l'exécuteur, assurez-vous de faire au moins une modification au code de la tentative envoyée *

La première tentative d'envoi d'un exercice peut nécessiter le téléchargement automatique de l'image de compilation et causer une erreur TIMEOUT. Après quelques minutes, la compilation devrait être fonctionnelle.

## Exécution des tests (facultatif)

### Tous les tests backend et l'analyse statique

- `docker compose run --rm be_tests`

### Uniquement certains tests backend

Il est possible de limiter l'exécution des tests à seulement ceux dont le nom complèt (Classe::test_nom_du_test) contient une chaîne :

- `docker compose run --rm be_tests <chaine>`

## Exécution de l'analyse statique PHPStan (facultatif)

- `docker compose run --rm be_phpstan`

## Exécution du linter (facultatif)

### backend

- `docker compose run --rm be_lint`

### frontend

- `docker compose run --rm fe_lint`

## Production de la documentation (facultatif)

- `docker compose up doc`

## En cas de problèmes :

- Voir les [FAQ](https://progression.pages.dti.crosemont.quebec/documentation/développement/faq.html)
